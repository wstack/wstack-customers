const functions = require('firebase-functions')
const admin = require('firebase-admin')
const express = require('express')

admin.initializeApp({
  credential: admin.credential.applicationDefault()
})

// Based on https://github.com/firebase/functions-samples/blob/master/authorized-https-endpoint/functions/index.js

const app = express()
app.use(validateFirebaseIdToken)
app.get('/hello', (req, res) => {
  res.send(`Hello ${req.user.name}`)
})

// This HTTPS endpoint can only be accessed by your Firebase Users.
// Requests need to be authorized by providing an `Authorization` HTTP header
// with value `Bearer <Firebase ID Token>`.
exports.app = functions.https.onRequest(app)

exports.getCustomers = functions.https.onRequest((request, response) => {
  response.json([
    {
      id: 1,
      name: 'Wysiwyg Oy'
    }
  ])
})

// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
function validateFirebaseIdToken (req, res, next) {
  console.log('Check if request is authorized with Firebase ID token')

  if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
    console.error(
      'No Firebase ID token was passed as a Bearer token in the Authorization header.',
      'Make sure you authorize your request by providing the following HTTP header:',
      'Authorization: Bearer <Firebase ID Token>',
      'or by passing a "__session" cookie.')
    res.status(403).send('Unauthorized')
    return
  }

  let idToken
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    console.log('Found "Authorization" header')
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split('Bearer ')[1]
  }

  admin.auth().verifyIdToken(idToken)
    .then(decodedIdToken => {
      console.log('ID Token correctly decoded', decodedIdToken)
      req.user = decodedIdToken
      return next()
    })
    .catch((error) => {
      console.error('Error while verifying Firebase ID token:', error)
      res.status(403).send('Unauthorized')
    })
}
